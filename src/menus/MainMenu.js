import React from 'react';
import MenuItem from '../components/MenuItem.js';
import { urls, basicAuth } from '../config';
import 'whatwg-fetch';

class MainMenu extends React.Component {
  state = {
    menu: []
  }

  constructor(props) {
    super(props);
    this.state.displayMainMenu = false;
    this.state.displayDesktop = false;
    this.state.didTranslate = false;
  }

  handleResize = () => {
    if(window.innerWidth > 1148) {
      this.setState({ displayMainMenu: true });
      this.setState({ displayDesktop: true });
    }

    else {
      if (this.state.displayDesktop === true) {
        this.setState({ displayDesktop: false });
        this.setState({ displayMainMenu: false });
      }
    }
  }

  handleLoad = () => {
    if(window.innerWidth < 1149) {
      this.setState({ displayMainMenu: false });
    }

    else {
      this.setState({ displayMainMenu: true});
    }
  }

  handleBurger = () => {
    this.state.displayMainMenu === false ? this.setState({ displayMainMenu: true }) : this.setState({ displayMainMenu: false });
  }

  render() {
    let displayClass = "closed";

    if (this.state.displayMainMenu === true) {
      displayClass = "open";
    }

    return(
      <React.Fragment>
      <div
        className="button-menu-mobile"
        onClick={this.handleBurger}
      >{ this.state.displayMainMenu === true ? ( <i className="fas fa-times"></i> ): ( <i className="fas fa-bars"></i> )}
      </div>
      { this.state.menu.items !== undefined ? (
        <ul className={displayClass + ' menu'}>
          {this.state.menu.items.map(function(menuItem, i) {
            return(
              <MenuItem key={i} item={menuItem} />
            )
          })}
        </ul>
      ): (null)}
      </React.Fragment>
    );
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
    window.removeEventListener('load', this.handleResize);
  }

  componentDidMount() {
    let url = urls.mainMenu;
    let headers = new Headers();
    if (basicAuth) {
      headers.set('Authorization', basicAuth);
    }

    window.fetch(url, {
      method:'GET',
      // mode: 'no-cors',
      headers: headers,
    })
    .then(res => res.json())
    .then((data) => {
      this.setState({ menu: data })
    })
    .catch(console.log);

    window.addEventListener('resize', this.handleResize);
    window.addEventListener('DOMContentLoaded', this.handleLoad);
  }

  componentDidUpdate() {
    // console.log("componentDidUpdate");
    var mainMenu = document.querySelector('#react-main-menu > ul.menu');

    if (this.state.didTranslate === false) {
      if (typeof(mainMenu) != 'undefined' && mainMenu != null) {
        this.setState({ didTranslate: true });
        this.props.translate();
      }
    }
  }
}

export default MainMenu;
