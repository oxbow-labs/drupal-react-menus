import React from 'react';
import {basicAuth, urls} from '../config';
import AlertMessage from '../components/AlertMessage.js';
import Cookies from 'js-cookie';
import 'whatwg-fetch';

const COOKIE_NAME = '_closed_alerts';

class Alerts extends React.Component {
  state = {
    alerts: []
  };

  constructor(props) {
    super(props);
  }

  closeAlert(id) {
    let closed = Cookies.get(COOKIE_NAME);
    closed = closed ? closed.split(',') : [];
    closed.push(id);
    let host = window.location.host.split('.');
    Cookies.set(COOKIE_NAME, closed.join(','), {
      domain: '.' + host.slice(host.length - 2).join('.')
    });
    this.setState({
      alerts: this.state.alerts.filter(a => a.id !== id)
    });
  }

  render() {
    const { alerts } = this.state;
    if (!alerts.length) return (<></>);
    const path = window.location.pathname;
    return alerts
      .filter(a => ((a.location === 'front' && path === '/') || a.location === 'all'))
      .map(a => (<AlertMessage key={a.id} alert={a} onClose={this.closeAlert.bind(this, a.id)} />));
  }

  componentDidMount() {
    let url = urls.alerts;
    let headers = new Headers();
    if (basicAuth) {
      headers.set('Authorization', basicAuth);
    }

    const closed = (Cookies.get(COOKIE_NAME) || '').split(',');

    window.fetch(url, {
      method:'GET',
      headers: headers,
    })
      .then(res => res.json())
      .then((data) => {
        this.setState({
          alerts: data.filter(a => !closed.includes(a.id.toString()))
        })
      })
      .catch(console.log);
  }
}

export default Alerts;
